using System;
using System.IO;
using System.Net.Sockets;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;

public struct Vec2
{
	public double x;
	public double y;
}

public class StartingPosition
{
	public Vec2 position;
	public double angle;
}

public class Dimension
{
	public double length;
	public double width;
	public double guideFlagPosition;
}

public class Car
{
	public CarId id;
	public Dimension dimensions;

	private Stats stats = new Stats();

	public Stats Stats { get { return stats; } }
}

public class Piece
{
	[JsonProperty]
	private double length;
	[JsonProperty("switch")]
	private bool isSwitch;

	[JsonProperty]
	private double radius;

	[JsonProperty]
	private double angle;

	private Track track;

	public double Angle
	{
		get { return angle; }
	}

	public void Link(Track t)
	{
		this.track = t;
	}

	public override string ToString ()
	{
		string len = "";
		string rad = "";
		for(int i=0;i<this.track.lanes.Count;i++)
		{
			if(len != "")
				len += ",";
			len += GetLength(i);

			if(rad != "")
				rad += ",";
			rad += GetRadius(i);
		}

		return string.Format ("[Piece l:({0}) s?:{1} r:{2} a:{3}]" , len, isSwitch, rad, angle);
	}


	public double GetLength(int startLane, int endLane)
	{
		// TODO: implement for a lane switch
		return Math.Max( GetLength(startLane) , GetLength(endLane) );
	}

	public double GetLength(int lane)
	{
		if(IsTurn)
		{
			return Math.PI * 2.0 * (this.GetRadius(lane)) * (Math.Abs(angle) / 360.0);
		}
		else
		{
			return length;
		}
	}

	public double GetRadius(double dist, int startLane, int endLane)
	{
		// TODO: implement radius for lane switch
		return Math.Max( GetRadius(startLane) , GetRadius(endLane) );
	}

	public double GetRadius(int lane)
	{
		return ((angle > 0 ? -1 : 1) * this.track.lanes[lane].distanceFromCenter) + radius;
	}

	public double GetCenterRadius()
	{
		return this.radius;
	}

	public bool IsTurn
	{
		get
		{
			return Math.Abs(angle) > 0 && radius > 0;
		}
	}
}

public class Lane
{
	public double distanceFromCenter;
	public int index;
}

public class Track
{
	public string id;
	public string name;
	public List<Piece> pieces;
	public List<Lane> lanes;
	
	private Race race;
	public Race Race
	{
		get { return race; }
	}

	public void Link(Race r)
	{
		this.race = r;
		foreach(Piece p in pieces)
		{
			p.Link(this);
		}
	}

	public override string ToString ()
	{
		string str = "";
		for(int i=0;i<pieces.Count;i++)
		{
			Piece p = pieces[i];
			str += " " + i.ToString() + ":" + p.ToString() + "\n";
		}
		return string.Format ("[Track {0} {1} {2}\n Pieces: \n{3}]" , this.id, this.name, this.lanes.Count, str);
	}
}

public class RaceSession
{
	public int laps;
	public int maxLapTimeMs;
	public bool quickRace;
}

public class Race
{
	public Track track;
	public List<Car> cars;
	public RaceSession raceSession;

	public void Link()
	{
		this.track.Link(this);
	}

	public override string ToString ()
	{
		return string.Format ("[Race {0}]" , track.ToString() );
	}

	public Lane GetLane(int lane)
	{
		return track.lanes[lane];
	}

	public double GetRadius(CarPosition cp)
	{
		Piece p = GetPiece(cp);
		return p.GetRadius(cp.piecePosition.inPieceDistance , cp.piecePosition.lane.startLaneIndex , cp.piecePosition.lane.endLaneIndex);
	}

	public Piece GetPiece(CarPosition cp)
	{
		return track.pieces[cp.piecePosition.pieceIndex];
	}

	public Piece GetNextPiece(CarPosition cp)
	{
		return track.pieces[(cp.piecePosition.pieceIndex + 1) % track.pieces.Count];
	}

	public double GetDistanceBetween(CarPosition start, CarPosition end)
	{
		Piece startP = GetPiece(start);
		Piece endP = GetPiece(end);
		if(endP == startP)
		{
			return end.piecePosition.inPieceDistance - start.piecePosition.inPieceDistance;
		}
		else
		{
			return GetDistanceTill(start, endP) + end.piecePosition.inPieceDistance;
		}
	}

	public double GetDistanceTill(CarPosition cp, Piece p)
	{

		double dist = 0f;
		int idx = cp.piecePosition.pieceIndex;
		while(p != track.pieces[idx])
		{
			dist += track.pieces[idx].GetLength(cp.piecePosition.lane.endLaneIndex ); //CalcLength(track.pieces[idx], cp.piecePosition.lane.endLaneIndex);

			if(idx == cp.piecePosition.pieceIndex)
			{
				dist -= cp.piecePosition.inPieceDistance;
			}
			idx = (idx + 1) % track.pieces.Count;
		}

		return dist;
	}

	public Piece GetNextTurnPiece(CarPosition cp)
	{
		int idx = cp.piecePosition.pieceIndex;

		idx = (idx + 1) % track.pieces.Count;
		while(idx != cp.piecePosition.pieceIndex)
		{
			if(track.pieces[idx].IsTurn)
			{
				return track.pieces[idx];
			}
			idx = (idx + 1) % track.pieces.Count;
		}

		DebugLog.WriteLine( "ERROR: Does this track only have one turn?" );
		return track.pieces[idx];
	}
}

public class LanePosition
{
	public int startLaneIndex;
	public int endLaneIndex;

	public LanePosition Clone()
	{
		LanePosition s = (LanePosition)this.MemberwiseClone();
		return s;
	}
}

public class PiecePosition
{
	public int pieceIndex;
	public double inPieceDistance;
	public LanePosition lane;
	public int lap;
	
	public override string ToString ()
	{
		return string.Format ("[PiecePosition idx={0} dist={1}]" , pieceIndex, inPieceDistance);
	}

	public PiecePosition Clone()
	{
		PiecePosition s = (PiecePosition)this.MemberwiseClone();
		s.lane = lane.Clone();
		return s;
	}
}

public class CarPosition
{
	public CarId id;
	public double angle;
	public PiecePosition piecePosition;
	
	public override string ToString ()
	{
		return string.Format ("[CarPosition color={0} pos={1} angle={2}]" , id.color , piecePosition , angle);
	}

	public CarPosition Clone()
	{
		CarPosition s = (CarPosition)this.MemberwiseClone();
		s.piecePosition = piecePosition.Clone();
		return s;
	}

	public void AddDistance(double d, Race currRace)
	{
		Piece p = currRace.GetPiece(this);
		piecePosition.inPieceDistance += d;
		double len = p.GetLength(this.piecePosition.lane.startLaneIndex, this.piecePosition.lane.endLaneIndex);
		while(piecePosition.inPieceDistance > len)
		{
			this.piecePosition.lane.startLaneIndex = this.piecePosition.lane.endLaneIndex;
			if( (piecePosition.pieceIndex + 1) >= currRace.track.pieces.Count)
			{
				piecePosition.lap++;
				piecePosition.pieceIndex=0;
			}
			else
			{
				piecePosition.pieceIndex++;
			}
			this.piecePosition.inPieceDistance = this.piecePosition.inPieceDistance - len; 
			p = currRace.GetPiece(this);
			len = p.GetLength(this.piecePosition.lane.endLaneIndex);
		}
	}
}


