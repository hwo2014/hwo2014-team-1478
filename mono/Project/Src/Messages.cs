using System;
using System.IO;
using System.Net.Sockets;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;


public class SendMsgWrapper {
	public string msgType;
	public Object data;
	public int gameTick;

	public SendMsgWrapper(string msgType, Object data, int tick) {
		this.msgType = msgType;
		this.data = data;
		this.gameTick = tick;
	}
}

public abstract class SendMsg {
	public int gameTick;

	public string ToJson() {
		return JsonConvert.SerializeObject(new SendMsgWrapper(this.MsgType(), this.MsgData(), this.gameTick));
	}
	protected virtual Object MsgData() {
		return this;
	}
	
	protected abstract string MsgType();
}

public class Join: SendMsg {
	public string name;
	public string key;
	public string color;
	
	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}
	
	protected override string MsgType() { 
		return "join";
	}
}

public class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

public class Throttle: SendMsg {
	public double value;
	
	public Throttle(double value) {
		this.value = value;
	}
	
	protected override Object MsgData() {
		return this.value;
	}
	
	protected override string MsgType() {
		return "throttle";
	}
}

public class Switch: SendMsg {
	public string value;
	
	public Switch(string value) {
		this.value = value;
	}
	
	protected override Object MsgData() {
		return this.value;
	}
	
	protected override string MsgType() {
		return "switchLane";
	}
}