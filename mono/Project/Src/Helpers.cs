using System;
using System.Collections;
using System.Collections.Generic;


public static class MathHelpers
{
	public static double AngleToRads(double angle)
	{
		return (angle / 360.0) * Math.PI * 2;
	}
	
	public static double RadsToAngle(double rads)
	{
		return (rads / (Math.PI * 2)) * 360.0;
	}
}


public static class StringHelpers
{
	public static string ArrayToString<T>(T[] arr)
	{
		string str = "";
		for(int i=0;i<arr.Length;i++)
		{
			if(str.Length != 0)
				str += ",";
			str += arr[i];
		}
		return str;
	}
	
	public static string DataRowToString<T>(T[,] data, int row)
	{
		string str = "";
		for(int i=0;i<data.GetLength(1);i++)
		{
			if(str.Length != 0)
				str += ",";
			str += data[row,i];
		}
		return str;
	}
}