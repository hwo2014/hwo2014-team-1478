using System;
using System.Collections;
using System.Collections.Generic;

public class LearningBot : Bot
{
	/*public enum Action
	{
		Breaking,
		Speeding
	}*/

	/*private Action currentAction = Action.Speeding;
	private double desiredVelocity = 0;
	private double desiredDistance = 0;
	private Stats startStopStats = null;*/

	public double crashAngle = 30f;

	// Centripetal Force -> Drift
	//private double maxCentriAccel = 0f;
	private double lastAngle = 0f;
	//private double throttleChangePerTick = 0.1f;

	private double currThrottle = 0.5;
	private double lastThrottle = 0.5;

	private Solver stoppingSolver = new Solver("StoppingSolver",5,2);
	//private Solver speedingSolver = new Solver(50,2);
	private Solver driftSolver = new Solver("DriftSolver",20,3);
	private Solver turnSolver = new Solver("TurnSolver",20,2);
	private const double MAX_DRIFT_ANGLE = 50;
	private Stats lastTurnRecord = null;
	private Stats lastDriftRecord = null;

	private int turnThrottle = 0;

	private bool foundStoppingPoint = false;
	//private Random rand = new Random();
	private const int MAX_NUM_THROTTLES = 2;
	public override void OnCrash(CrashResponse cr, int tick) 
	{ 
		DebugLog.WriteLine( string.Format("CRASH ANGLE: {0}" , lastAngle ) );
		crashAngle = lastAngle;
	}

	private List<Recorder> recorders = new List<Recorder>();

	public override void OnSpawn(SpawnResponse sp, int tick) { }

	/*private bool CanGetToNextStraightUnderAngle( Piece nextTurn , double initialVel , double initialAcceleration, double initialJerk, double maxAngle )
	{
		Piece startTurn;
		Stats stats = yourCar.Stats.Clone();
		stats.lastPosition = new CarPosition() { 
			id = yourCar.id, 
			angle = 0, 
			piecePosition = new PiecePosition() {
				pieceIndex = nextTurn.GetIndex(),
				inPieceDistance = 0,
				lane = new LanePosition() {
					startLaneIndex = stats.lastPosition.piecePosition.lane.endLaneIndex,
					endLaneIndex = stats.lastPosition.piecePosition.lane.endLaneIndex
				},
				lap = stats.lastPosition.piecePosition.lap
			}
		};
		stats.lastVelocity = initialVel;
		stats.lastAcceleration = intialAcceleration;
		stats.lastJerk = initialJerk;
		stats.lastDriftAngle = 0;
		stats.lastDriftVelocity = 0;
		stats.lastDriftAcceleration = 0;

		while(nextTurn.IsTurn)
		{
			turnSolver.Process(stats.lastVelocity,stats.lastAcceleration);

		}
	}*/

	public override BotCommand Update (List<CarPosition> poses, int tick)
	{

		Stats stats = yourCar.Stats;
		CarPosition currPos = yourCar.Stats.currPosition;

		if(currPos == null)
		{
			DebugLog.WriteLine( "Skipped car position tick!" );
			return BotCommand.Throttle(0.5f);
		}
		else
		{
			Piece currPiece = currentRace.GetPiece(currPos);
			Piece lastPiece = stats.lastPosition == null ? null : currentRace.GetPiece(stats.lastPosition);

			/*if(turnSolver.FoundSolution && driftSolver.FoundSolution && stoppingSolver.FoundSolution)
			{

				if(!currPiece.IsTurn)
				{
					if(!foundStoppingPoint)
					{
						Piece nextTurn = currentRace.GetNextTurnPiece(currPos);
						double finalVelocityIfStop = stoppingSolver.Process( stats.currVelocity , currentRace.GetDistanceTill( currPos , nextTurn  ) );

							if( CanGetToNextStraightUnderAngle( nextTurn, finalVelocityIfStop ) >= 45 )
						{
							foundStoppingPoint = true;
							currThrottle = 0;
						}
						else
						{
							currThrottle = 1;
						}
					}
					else
					{
						currThrottle = 0;
					}
				}
				else
				{
					foundStoppingPoint = false;
					currThrottle = 1;
				}

			}
			else*/
			{

				if(currPiece.IsTurn)
				{
					if(lastPiece != currPiece)
					{
						turnThrottle++;
					}

					double lastDist = currentRace.GetDistanceBetween(stats.lastPosition, stats.currPosition);
					double lastCentriAccel = (stats.lastVelocity * stats.lastVelocity) / currPiece.GetRadius(stats.lastPosition.piecePosition.lane.endLaneIndex);
					double lastVel = stats.lastVelocity;
					double lastAccel = stats.lastAcceleration;

					if(recorders.Count > 0 && !recorders[recorders.Count-1].HasStopped)
					{
						recorders.RemoveAt( recorders.Count-1 );
					}

					if(turnSolver.FoundSolution)
					{
						DebugLog.WriteLine( string.Format("COMPARE TURN DISTANCE, COMPUTED: {0} vs REAL: {1}" ,
								currentRace.GetDistanceBetween(stats.lastPosition, stats.currPosition) , 
						        turnSolver.Process(
							lastVel,
							lastAccel
						                   ) ) );

					}
					else if( (lastPiece == currPiece) && (lastTurnRecord == null || Math.Abs(stats.currVelocity - lastTurnRecord.currVelocity) > 0.05) )
					{
						lastTurnRecord = stats.Clone();

						DebugLog.WriteLine( string.Format( "ADD DATA TO TURN SOLVER: D = f( v, a ) where D = {0} v = {1} a = {2}" , 
						                                  lastDist , lastVel , lastAccel ) );
						turnSolver.AddData( 
							lastDist, 
						    lastVel, 
						    lastAccel );

						if(turnSolver.IsAtMax)
						{
							DebugLog.WriteLine( "SOLVE TURN!" );
							turnSolver.Solve();
						}
					}


					if(driftSolver.FoundSolution)
					{
						DebugLog.WriteLine( string.Format("COMPARE DRIFT ANGLE, COMPUTED: {0} vs REAL: {1}" ,
						                                  stats.currDriftAngle - stats.lastDriftAngle , 
						                                  driftSolver.Process(
							stats.lastDriftVelocity,
							stats.lastDriftAcceleration,
							stats.lastVelocity
							) ) );
						
					}
					else if( (lastPiece == currPiece) && stats.currDriftAngle > 0.1 && (lastDriftRecord == null || (stats.currDriftAngle - lastDriftRecord.currDriftAngle) > 1 ) )
					{
						lastDriftRecord = stats.Clone();
						
						DebugLog.WriteLine( string.Format( "ADD DATA TO DRIFT SOLVER: Theta = f( w , fish ) where Theta = {0} w = {1} fish = {2}" , 
						                                  (stats.currDriftAngle - stats.lastDriftAngle) , stats.lastDriftVelocity , stats.lastDriftAcceleration ) );
						driftSolver.AddData( (stats.currDriftAngle - stats.lastDriftAngle) , stats.lastDriftVelocity , stats.lastDriftAcceleration , stats.lastVelocity );
						
						if(driftSolver.IsAtMax)
						{
							DebugLog.WriteLine( "SOLVE DRIFT!" );
							driftSolver.Solve();
						}
					}

					currThrottle = 0.7; //((turnThrottle % 3) + 5) / 10.0;
				}
				else
				{
					if(!stoppingSolver.FoundSolution)
					{
						// LEARN TO STOP
						if(recorders.Count == 0 )
						{
							recorders.Add( new Recorder() );
						}

						Recorder currRecorder = recorders[recorders.Count-1];
						if(currRecorder.HasStopped)
						{
							recorders.Add( new Recorder() );
							currRecorder = recorders[recorders.Count-1];
						}

						if(!currRecorder.HasStarted)
						{
							if(stats.currVelocity > ((recorders.Count + 1) % 5))
							{
								currRecorder.Start(stats);
								currThrottle = 0.0;
							}
							else
							{
								currThrottle = 1.0;
							}
						}
						else
						{
							if( stats.currVelocity < (0.5 * currRecorder.StartStats.currVelocity) )
							{
								currRecorder.Stop(stats);
								double dist = currRecorder.ComputeDistance( currentRace );

								DebugLog.WriteLine( string.Format("RECORDED STOP DATA: {0} -> {1} ", currRecorder.StartStats , currRecorder.StopStats ) ); 

								DebugLog.WriteLine( string.Format( "RECORD COMPUTED: Deccel = {0} Dist = {1}" , currRecorder.ComputeAcceleration() , dist ) );

								DebugLog.WriteLine( string.Format( "ADD DATA TO STOPPING SOLVER: Vf = f( Vi , D ) where Vf = {0} Vi = {1} D = {2}" , 
								                                  currRecorder.StopStats.currVelocity , currRecorder.StartStats.currVelocity , dist ) );
								stoppingSolver.AddData( currRecorder.StopStats.currVelocity , currRecorder.StartStats.currVelocity , dist );
								stoppingSolver.Solve();

								DebugLog.WriteLine( "COMPARE SOLUTIONS ..." );

								if(stoppingSolver.FoundSolution)
								{
									foreach(Recorder r in recorders)
									{
										double rDist = r.ComputeDistance(currentRace);
										DebugLog.WriteLine( string.Format( "Vf = f( Vi , D ) = f( {0} , {1} ) = {2}  (Vf from recorder = {3})" , 
										                                  r.StartStats.currVelocity,
										                                  rDist,
										                                  stoppingSolver.Process( r.StartStats.currVelocity , rDist ) , r.StopStats.currVelocity ) );
									}
								}

								DebugLog.WriteLine( "COMPARE DONE!" );

								currThrottle = 0.0;
							}
							else
							{
								currThrottle = 0.0;
							}
						}
					}
					else
					{
						currThrottle = 0.55;
						/*if(stats.currVelocity < 6)
						{
							currThrottle = lastThrottle + 0.1;
						}
						else
						{
							currThrottle = lastThrottle - 0.1;
						}*/
					}
				}
			}

			DebugLog.WriteLine( string.Format( "THROTTLE: {0}" , currThrottle ) );

			lastThrottle = currThrottle;
			
			return BotCommand.Throttle(currThrottle);
		}
	}

	
}





						/*if(currentAction == Action.Breaking)
			{
				currThrottle = 0;
				if( (stats.currVelocity - desiredVelocity) < 0.01 || currPiece.IsTurn)
				{
					double dist = currentRace.GetDistanceBetween(startStopStats.currPosition , currPos);
					DebugLog.WriteLine( string.Format( "START STATS: {0} DESIRED VELOCITY: {1} DESIRED DISTANCE: {2}" , startStopStats.ToString() , desiredVelocity, desiredDistance ) );
					DebugLog.WriteLine( string.Format( "Vf = f( Vi , D ) where Vf = {0} Vi = {1} D = {2}" , stats.currVelocity , startStopStats.currVelocity , dist ) );
					if(dist > 0.5 )
					{
						stoppingSolver.AddData( stats.currVelocity , startStopStats.currVelocity , dist );
						stoppingSolver.Solve();
					}
					currentAction = Action.Speeding;
					currThrottle = 1;
				}
			}
			else*/ 
						/*else
				{
					Piece nextPiece = currentRace.GetNextTurnPiece(currPos);
						
					double dist = currentRace.GetDistanceTill(currPos , nextPiece);
					double stopVel = CalculateStoppingVelocity( stats.currVelocity , dist  );
					double driftAngle = CalculateMaxDriftAngle(stopVel , nextPiece , currPos.piecePosition.lane.endLaneIndex);

					DebugLog.WriteLine( string.Format( "POTENTIAL STOPPING VEL: {0} DRIFT ANGLE AT THAT VELOCITY {1}" , stopVel , driftAngle ) );
					if(driftAngle > MAX_DRIFT_ANGLE)
					{
						currThrottle = 0;
						currentAction = Action.Breaking;
						desiredVelocity = stopVel;
						desiredDistance = dist;
						startStopStats = stats.Clone();
					}
					else
					{
						currThrottle = 1;
						currentAction = Action.Speeding;
					}
				}*/
						/*public double CalculateSpeedingVelocity( double vInitial, double dist )
	{
		if( speedingSolver.NumDataPoints == 0 )
		{
			// Guess
			return Math.Sqrt( (vInitial * vInitial) + (2 * -0.1 * dist) );
		}
		else
		{
			return speedingSolver.Process( vInitial , dist );
		}
	}*/
						
						/*public double CalculateStoppingVelocity( double vInitial, double dist )
	{
		if( stoppingSolver.FoundSolution )
		{
			// Guess
			return Math.Sqrt( (vInitial * vInitial) + (2 * -0.1 * dist) );
		}
		else
		{
			return stoppingSolver.Process( vInitial , dist );
		}
	}

	public double CalculateMaxDriftAngle( double vInitial , Piece turn, int lane )
	{
		if( !driftSolver.FoundSolution  )
		{
			return (turn.GetLength(0) / vInitial) * 15f;
		}
		else
		{
			return driftSolver.Process( vInitial , turn.GetRadius(lane) );
		}
	}*/
/*
				double vFinal = stats.currVelocity;
				double vMin = 0;
				double vMax = -1;
				for(int i=0;i<5;i++)
				{
					driftAngle = CalculateMaxDriftAngle( vFinal );
					DebugLog.WriteLine( string.Format( "CALC: {0} => {1}" , vFinal , driftAngle ) );
					if(driftAngle < 60.0)
					{
						if(vMax < 0)
						{
							vMin = vFinal;
							vFinal *= 2;
						}
						else
						{
							vFinal = (vMax - vMin) / 2;
						}
					}
					else
					{
						vMax = vFinal;
						vFinal = (vMax - vMin) / 2;
					}
				}
				*/

/*if(stats.lastTick > 0)
			{
				if(!currPiece.IsTurn)
				{
					if(straightSolver.FoundSolution)
					{
						for(int i=0;i<MAX_NUM_THROTTLES;i++)
						{
							double th = ((double)i / (MAX_NUM_THROTTLES-1));
							double res = straightSolver.Process( th , stats.currVelocity , stats.currAcceleration , stats.currJerk );
							DebugLog.WriteLine( string.Format( "SOLVED: f({1} , {2} , {3}, {4}) => {0}" , res, th, stats.currVelocity , stats.currAcceleration , stats.currJerk ) );
						}
					}
					else if(straightSolver.IsAtMax)
					{
						DebugLog.WriteLine( "SOLVE IT!" );
						straightSolver.Solve();
					}
					else
					{
						straightSolver.AddData( stats.currVelocity, lastThrottle, stats.lastVelocity, stats.lastAcceleration , stats.lastJerk );
					}
				}
				else
				{
					if(turnSolverVel.IsAtMax)
					{
						if(!turnSolverVel.FoundSolution)
						{
							turnSolverVel.Solve();
						}
					}
					else
					{
						turnSolverVel.AddData( stats.currVelocity, lastThrottle, stats.lastVelocity, this.currentRace.GetRadius( currPos ));
					}


					if(turnSolverAngle.IsAtMax)
					{
						if(!turnSolverAngle.FoundSolution)
						{
							turnSolverAngle.Solve();
						}
					}
					else
					{
						turnSolverAngle.AddData( currPos.angle, lastThrottle, stats.lastVelocity, this.currentRace.GetRadius( currPos ));
					}
				}
			}*/

/*lastAngle = currPos.angle;
			if(ticksBeforeThrottleChange <= 0)
			{
				ticksBeforeThrottleChange = 3;
				currThrottle = ((double)rand.Next(0,MAX_NUM_THROTTLES)) / (MAX_NUM_THROTTLES-1);
			}
			else 
			{
				ticksBeforeThrottleChange--;
				currThrottle = lastThrottle;
			}*/

/*
			if(currPiece.IsTurn)
			{
				double radius = this.currentRace.GetRadius( currPos );
				double centriAccel = (stats.currVelocity * stats.currVelocity) / radius;

				DebugLog.WriteLine( string.Format( "RADIUS: {0}" , radius ) );
				DebugLog.WriteLine( string.Format( "CENTRI ACCEL: {0} DRIFT: {1}" , centriAccel , currPos.angle ) );

				if(maxCentriAccel > centriAccel)
				{
					maxCentriAccel = centriAccel;
					DebugLog.WriteLine( string.Format( "NEW MAX! CENTRI ACCEL: {0} DRIFT: {1}" , maxCentriAccel , currPos.angle ) );
				}

				if(currPos.angle < crashAngle)
				{
					currThrottle = Math.Min( 1f, currThrottle + throttleChangePerTick );
				}
				else
				{
					currThrottle = Math.Max( 0f, currThrottle - throttleChangePerTick );
				}

			}
			else
			{
				currThrottle = Math.Min( 1f, currThrottle + throttleChangePerTick );
			}
			*/



