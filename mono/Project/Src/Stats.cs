// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;


public class Stats
{
	public double currTick;
	public double lastTick;

	public double currVelocity;
	public double lastVelocity;
	
	public CarPosition lastPosition;
	public CarPosition currPosition;
	
	public double currAcceleration;
	public double lastAcceleration;

	public double currJerk;
	public double lastJerk;

	public double currDriftAngle;
	public double lastDriftAngle;

	public double currDriftVelocity;
	public double lastDriftVelocity;

	public double currDriftAcceleration;
	public double lastDriftAcceleration;

	public int currDriftSign;
	public int lastDriftSign;

	public void BeforeUpdate(Bot bot, CarPosition pos, int tick)
	{
		currPosition = pos;
		if(tick != lastTick)
		{
			currVelocity = bot.CalculateVelocity(currPosition, lastPosition, tick, lastTick);
			currAcceleration = (currVelocity - lastVelocity) / (double) (tick - lastTick);
			currJerk = (currAcceleration - lastAcceleration) / (double) (tick - lastTick);
			currTick = tick;



			currDriftSign = Math.Sign(pos.angle);
			currDriftAngle = pos.angle; //* (bot.YourCar.dimensions.length - bot.YourCar.dimensions.guideFlagPosition);
			currDriftVelocity = (currDriftAngle - lastDriftAngle) / (double) (tick - lastTick);
			currDriftAcceleration = (currDriftVelocity - lastDriftVelocity) / (double) (tick - lastTick);

			if(lastDriftSign != currDriftSign)
			{
				DebugLog.WriteLine( string.Format( "DRIFT SIGN CHANGE: {0} {1} {2}" , 
				                                  lastDriftAngle,
				                                  currDriftAngle,
				                                  bot.CurrentRace.GetPiece( currPosition ).GetRadius(0) ) );
			}
		}
		
		DebugLog.WriteLine( string.Format("{0}> VELOCITY: {1} ACCEL: {2} JERK: {3} D: {4} DVEL: {5} DACCEL: {6}" , 
		                                  pos.id.color , currVelocity , currAcceleration, currJerk,
		                                  currDriftAngle , currDriftVelocity, currDriftAcceleration) );
	}
	
	public void AfterUpdate(CarPosition pos, int tick)
	{
		lastPosition = currPosition;
		lastTick = tick;
		lastVelocity = currVelocity;
		lastAcceleration = currAcceleration;
		lastJerk = currJerk;

		lastDriftSign = currDriftSign;
		lastDriftAngle = currDriftAngle;
		lastDriftVelocity = currDriftVelocity;
		lastDriftAcceleration = currDriftAcceleration;


	}

	public Stats Clone()
	{
		Stats s = (Stats)this.MemberwiseClone();
		return s;
	}

	public override string ToString ()
	{
		return string.Format ("[Stats (Tick {0}) (Pos: {1}) (Vel: {2}) (Acc: {3})" , currTick , currPosition , currVelocity, currAcceleration );
	}
}

