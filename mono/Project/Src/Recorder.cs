using System;
using System.Collections;
using System.Collections.Generic;

public class Recorder
{
	private Stats start = null;
	private Stats stop = null;
	
	public Stats StartStats
	{
		get { return start; }
	}
	
	public Stats StopStats
	{
		get { return stop; }
	}
	
	public bool HasStarted
	{
		get { return start != null; }
	}
	
	public bool HasStopped
	{
		get { return stop != null; }
	}
	
	public void Start(Stats s)
	{
		start = s.Clone();
	}
	
	public void Stop(Stats s)
	{
		stop = s.Clone();
	}
	
	public double ComputeAcceleration()
	{
		if(stop.currTick == start.currTick)
		{
			return 0;
		}
		else
		{
			return (stop.currVelocity - start.currVelocity) / (stop.currTick - start.currTick);
		}
	}
	
	public double ComputeDistance(Race currentRace)
	{
		return currentRace.GetDistanceBetween(start.currPosition , stop.currPosition);
	}
}