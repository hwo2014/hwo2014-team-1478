using System;
using System.Collections;
using System.Collections.Generic;


public class LearningBot2 : Bot
{
	public delegate double GetThrottleFunc(CarPosition pos);
	public delegate bool StopIteratingFunc(CarPosition pos,double vel);
	/*public enum Action
	{
		Breaking,
		Speeding
	}*/
	
	/*private Action currentAction = Action.Speeding;
	private double desiredVelocity = 0;
	private double desiredDistance = 0;
	private Stats startStopStats = null;*/
	
	public double crashAngle = 30f;
	
	// Centripetal Force -> Drift
	//private double maxCentriAccel = 0f;
	private double lastAngle = 0f;
	//private double throttleChangePerTick = 0.1f;
	
	private double nextThrottle = 0.5;
	private double lastThrottle = 0.5;
	
	private Solver straightSolver = new Solver("StraightSolver",50,2);
	//private Solver speedingSolver = new Solver(50,2);
	private Solver driftSolver = new Solver("DriftSolver",500,2);
	private Solver turnSolver = new Solver("TurnSolver",50,2);
	private const double MAX_DRIFT_ANGLE = 50;

	//private Random rand = new Random();
	private const int MAX_NUM_THROTTLES = 2;
	private const int MAX_TICKS_TO_ITERATE = 500;

	private bool stillLearning = true;
	private int lastRecordedTick = 0;

	public override void OnCrash(CrashResponse cr, int tick) 
	{ 
		DebugLog.WriteLine( string.Format("CRASH ANGLE: {0}" , lastAngle ) );
		crashAngle = lastAngle;
	}

	public override void OnSpawn(SpawnResponse sp, int tick) { }
	
	private void Iterate(Stats startingPoint, GetThrottleFunc getThrottleFunc, StopIteratingFunc stopIteratingFunc)  // Piece nextTurn , double initialVel , double initialAcceleration, double initialJerk, double maxAngle )
	{
		DebugLog.WriteLine( "START ITERATE: " + startingPoint.currPosition.ToString() );
		string str= "";
		int ticks = 0;
		double vel = startingPoint.currVelocity;
		double accel = startingPoint.currAcceleration;
		CarPosition pos = startingPoint.currPosition.Clone();

		int lane = startingPoint.currPosition.piecePosition.lane.endLaneIndex;
		double driftVel = startingPoint.currDriftVelocity;
		double driftAccel = startingPoint.currDriftAcceleration;
		double drift = startingPoint.currDriftAngle;
		do
		{
			Piece currPiece = currentRace.GetPiece(pos);
			
			double throttle = getThrottleFunc(pos);
			str += throttle.ToString() + " : ";
			double centriAccel = currPiece.IsTurn ? (vel * vel) / currPiece.GetRadius(lane) : 0;

			//driftAccel = driftSolver.Process( driftVel, driftAccel, Math.Sin( MathHelpers.AngleToRads(drift) ) * vel );
			drift = driftSolver.Process( currPiece.GetRadius(pos.piecePosition.lane.endLaneIndex), vel );
			//driftVel += driftAccel;
			//drift += driftVel;
			pos.angle = drift;

			if(currPiece.IsTurn)
			{
				accel = turnSolver.Process(throttle, vel);
            }
            else
            {
				accel = straightSolver.Process(throttle, vel );
			}


			vel += accel;
			pos.AddDistance( vel, this.currentRace );

			ticks++;
				
		} while( !stopIteratingFunc(pos,vel) && MAX_TICKS_TO_ITERATE > ticks );

		DebugLog.WriteLine( str );
		DebugLog.WriteLine( "END ITERATE: " + pos.ToString() + " TICKS: " + ticks.ToString() );

	}
				
	public override BotCommand Update (List<CarPosition> poses, int tick)
	{
		
		Stats stats = yourCar.Stats;
		CarPosition currPos = yourCar.Stats.currPosition;
		
		if(currPos == null)
		{
			DebugLog.WriteLine( "Skipped car position tick!" );
			return BotCommand.Throttle(0.5f);
		}
		else
		{
			Piece currPiece = currentRace.GetPiece(currPos);
			Piece lastPiece = stats.lastPosition == null ? null : currentRace.GetPiece(stats.lastPosition);
			
			/*if(turnSolver.FoundSolution && straightSolver.FoundSolution && driftSolver.FoundSolution )
			{


				if(currPiece.IsTurn)
				{
					if( Math.Abs(currPos.angle) > 45 )
					{
						nextThrottle = 0;
					}
					else
					{
						nextThrottle = 1;
					}
				}
				else
				{
					double maxAngle = 0;
					Piece lastTurn = null;

					Iterate( stats, (pos) => {
						if(currentRace.GetPiece(pos).IsTurn)
						{
							return Math.Abs(pos.angle) < 45 ? 1 : 0;
						}
						else
						{
							return 0;
						}
					} , (pos,vel) => {
						maxAngle = Math.Max( maxAngle , Math.Abs(pos.angle) );

						if(lastTurn != null)
						{
							return !currentRace.GetPiece(pos).IsTurn;
						}
						else
						{
							Piece p = currentRace.GetPiece(pos);
							if(p.IsTurn)
								lastTurn = p;
							return false;
						}
					});

					DebugLog.WriteLine( "ESTIMATE ANGLE: " + maxAngle );
					if(maxAngle < 80)
						nextThrottle = 1;
					else
						nextThrottle = 0;
				}

			}
			else*/
			{

				if(currPiece.IsTurn)
				{
					if( Math.Abs(currPos.angle) > 5)
					{
						nextThrottle = 0;
					}
					else
					{
						nextThrottle = 1;
					}
				}
				else
				{
					Piece nextTurn = currentRace.GetNextTurnPiece(currPos);
					double distToTurn = currentRace.GetDistanceTill( currPos, nextTurn );
					if(distToTurn < 100 && stats.currVelocity > 2)
					{
						nextThrottle = 0;
					}
					else
					{
						nextThrottle = 1;
					}
				}

				double lastCentriAccel = (currPiece.IsTurn ? (stats.lastVelocity * stats.lastVelocity) / lastPiece.GetRadius(stats.lastPosition.piecePosition.lane.startLaneIndex) : 0);
				double currAccel = stats.currAcceleration;

				//nextThrottle = ((int)( (tick+50)/50.0)) % 2;

				if(lastPiece == currPiece && stats.lastDriftSign == stats.currDriftSign)
				{
					if(currPiece.IsTurn)
					{
						if(tick % 2 == 0)
							UpdateSolver( turnSolver , currAccel , lastThrottle, stats.lastVelocity );
					}
					else
					{
						if(tick % 2 == 0)
							UpdateSolver( straightSolver , currAccel, lastThrottle , stats.lastVelocity );//, stats.lastVelocity * stats.lastVelocity , stats.lastVelocity );
					}

					if(stats.currDriftAngle > 1 && currPiece.IsTurn)
					{
						if(tick % 3 == 0)
							UpdateSolver( driftSolver , stats.currDriftAngle , lastPiece.GetRadius(stats.lastPosition.piecePosition.lane.endLaneIndex), stats.lastVelocity /*Math.Sin( MathHelpers.AngleToRads(stats.lastDriftAngle) * */   );

						if(!driftSolver.FoundSolution && currPos.piecePosition.lap >= 1)
						{
							driftSolver.Solve();
						}
					}
				}
			}
			
			DebugLog.WriteLine( string.Format( "THROTTLE: {0}" , nextThrottle ) );
			
			lastThrottle = nextThrottle;
			
			return BotCommand.Throttle(nextThrottle);
		}
	}


	private static void UpdateSolver(Solver solver, double output, params double[] inputs)
	{
		if(solver.FoundSolution)
		{
			DebugLog.WriteLine( string.Format("COMPARE {0}, COMPUTED: {1} vs REAL: {2}" ,
			                                  solver.name,
			                                  solver.Process( inputs ), output ) );
		}
		else
		{
			string str = "";
			foreach(double d in inputs) { if(str.Length != 0) {str += ",";} str += d.ToString(); }
			DebugLog.WriteLine( string.Format( "ADD DATA TO {0} SOLVER: result = f( inputs ) where {1} = f( {2} )" , solver.name, output , str ) );

			solver.AddData( output, inputs );
			
			if(solver.IsAtMax)
			{
				DebugLog.WriteLine( string.Format("SOLVE FOR {0}!", solver.name) );
				solver.Solve();
			}
		}
	}
	
}
				
				
				
