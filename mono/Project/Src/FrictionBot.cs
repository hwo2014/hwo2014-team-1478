using System;
using System.Collections;
using System.Collections.Generic;

public class FrictionBot : Bot
{
	private enum FrictionState
	{
		Unknown,
		Calculating,
		Found
	}

	public const int TICKS_TO_LET_GO = 20;
	public const int TICKS_TO_WAIT = 20;
	
	//private int tickWhenCarLetGo = 0;
	//private double velWhenCarLetGo = 0;
	private CarPosition posWhenCarLetGo;
	private double frictionTimesGravity = 0.1f;

	private FrictionState frictionState = FrictionState.Unknown;
	private int tickWhenFictionWasUnknown = 0;
	//private int foundFriction;
	//private double fricStartingVel = 0f;

	public override void OnCrash(CrashResponse cr, int tick) { }
	
	public override void OnSpawn(SpawnResponse sp, int tick) { }
	
	public override BotCommand Update (List<CarPosition> poses, int tick)
	{
		DebugLog.WriteLine( "TICK : " + tick.ToString() );
		CarPosition currPos = yourCar.Stats.currPosition;

		if(currPos == null)
		{
			DebugLog.WriteLine( "Skipped car position tick!" );
			return BotCommand.Throttle(0.5f);
		}
		else
		{
			double nextThrottle = 0.5f;
			double currVel = yourCar.Stats.currVelocity;
			Piece currPiece = currentRace.GetPiece(currPos);

			if(currVel == 0f)
			{
				nextThrottle = 0.5f;
			}
			else if(frictionState == FrictionState.Unknown)
			{ 
				DebugLog.WriteLine( "FRICTION UKNOWN: " + tickWhenFictionWasUnknown.ToString() + " : " + TICKS_TO_WAIT.ToString() );
				if( (tick - tickWhenFictionWasUnknown) < TICKS_TO_WAIT )
				{
					nextThrottle = 0.5f;
				}
				else
				{
					frictionState = FrictionState.Calculating;
					nextThrottle = 0f;
					//tickWhenCarLetGo = tick;
					//velWhenCarLetGo = currVel;
					posWhenCarLetGo = currPos;
				}
			}
			else if(frictionState == FrictionState.Calculating)
			{
				DebugLog.WriteLine( "FRICTION CALCULATING!" );
				if( /*(tick - tickWhenCarLetGo) > TICKS_TO_LET_GO ||*/ currPiece.IsTurn || currPiece != currentRace.GetPiece(posWhenCarLetGo) || currVel <= 0.01f )
				{
					nextThrottle = 1f;
					frictionState = FrictionState.Found;
				}
				else
				{
					nextThrottle = 0f;
					frictionTimesGravity = Math.Abs(yourCar.Stats.currAcceleration);
					//frictionTimesGravity = System.Math.Abs( ((currVel * currVel) - (velWhenCarLetGo * velWhenCarLetGo)) / ( 2 * (currPos.piecePosition.inPieceDistance - posWhenCarLetGo.piecePosition.inPieceDistance)) );
					DebugLog.WriteLine( string.Format("FRICTION x GRAVITY = {0}" , frictionTimesGravity) );
				}
			}
			else
			{

				if(currPiece.IsTurn)
				{
					double maxVel = (double)System.Math.Sqrt(frictionTimesGravity * currPiece.GetCenterRadius());
					DebugLog.WriteLine( string.Format( "MAX VELOCITY THRU TURN: {0}" , maxVel) );

					if(currVel > maxVel)
					{
						nextThrottle = 0f;
					}
					else
					{
						nextThrottle = 1f;
					}
				}
				else
				{
					Piece nextTurn = currentRace.GetNextTurnPiece(currPos);
					double maxVel = (double)System.Math.Sqrt(frictionTimesGravity * nextTurn.GetCenterRadius());
					DebugLog.WriteLine( string.Format( "MAX VELOCITY THRU NEXT TURN: {0}" , maxVel) );

					DebugLog.WriteLine( "NEXT TURN: " + nextTurn.ToString() );
					double distToNextTurn = currentRace.GetDistanceTill( currPos, nextTurn );
					DebugLog.WriteLine( "DIST TO NEXT TURN: " + distToNextTurn.ToString() );

					double distNeeded = ((maxVel * maxVel) - (currVel * currVel)) / ( 2 * frictionTimesGravity );
					if(distNeeded < 0 && Math.Abs(distNeeded) > distToNextTurn)
					{
						nextThrottle = 0f;
					}
					else
					{
						nextThrottle = 1f;
					}

				}
			}

			return BotCommand.Throttle(nextThrottle);
		}
	}
}


