using System;
using System.Collections;
using System.Collections.Generic;


public class Solver
{
	public string name;
	private bool foundSolution = false;
	private int nPoints = 0;
	double[,] data;
	double maxDistanceBetweenElements = 0;
	
	private int nInputs;
	private alglib.linreg.linearmodel lm;
	
	public Solver(string name, int maxPoints, int nInputs)
	{
		this.name = name;
		data = new double[maxPoints , nInputs + 1];
		this.nInputs = nInputs;
	}
	
	public double[,] Data
	{
		get
		{
			return data;
		}
	}
	
	private double GetDistance(int row, double output, double[] input)
	{
		double sum = 0;
		for(int i=0;i<this.nInputs;i++)
		{
			double d = (data[row,i] - input[i]);
			sum += d * d;
		}
		double d2 = (data[row,this.nInputs] - output);
		sum += d2 * d2;
		return Math.Sqrt(sum);
	}
	
	public void AddData(double output, params double[] input)
	{
		if(input.Length != this.nInputs)
		{
			throw new System.Exception( "Not correct data!" );
		}
		else if(nPoints == data.GetLength(0))
		{
			DebugLog.WriteLine( "Max data logged!" );
			return;
		}
		else
		{
			/*bool isMax = false;
			bool foundEqual = false;
			for(int r=0;r<this.nPoints;r++)
			{
				double dist = GetDistance(r,output,input);
				if(dist < 0.001)
				{
					foundEqual = true;
					DebugLog.WriteLine( string.Format("DATA EQUAL: {0} vs {1},{2}" , StringHelpers.DataRowToString<double>(data,r) , StringHelpers.ArrayToString(input), output) );
				}
				
				if(dist > maxDistanceBetweenElements)
				{
					maxDistanceBetweenElements = dist;
					isMax = true;
				}
			}
			
			if(isMax || !foundEqual)
			{*/
				for(int i=0;i<this.nInputs;i++)
				{
					data[nPoints,i] = input[i];
				}
				data[nPoints,this.nInputs] = output;
				nPoints++;
			/*}
			else
			{
				DebugLog.WriteLine( "Could not add data!" );
			}*/
		}
	}
	
	public int NumDataPoints
	{
		get {
			return this.nPoints;
		}
	}
	
	public bool IsAtMax
	{
		get
		{
			return nPoints == data.GetLength(0);
		}
	}
	
	public bool FoundSolution { get { return foundSolution; } }
	
	public void Solve()
	{
		if((this.nInputs+1) >= this.nPoints)
		{
			return;
		}
		
		double[,] nData;
		if(nPoints != data.GetLength(0))
		{
			nData = new double[nPoints, nInputs + 1];
			for(int i=0;i<this.nPoints;i++)
			{
				for(int j=0;j<(this.nInputs+1);j++)
				{
					nData[i,j] = data[i,j];
				}
			}
		}
		else
		{
			nData = data;
		}
		
		for(int i=0;i<nData.GetLength(0);i++)
		{
			string str = "";
			for(int j=0;j<nData.GetLength(1);j++)
			{
				str += nData[i,j].ToString() + ", ";
			}
			DebugLog.WriteLine( "Data: " + str );
		}
		
		foundSolution = true;
		lm = new alglib.linreg.linearmodel();
		alglib.linreg.lrreport ar = new alglib.linreg.lrreport();
		int info = 0;
		alglib.linreg.lrbuild( nData , this.nPoints , this.nInputs , ref info , lm, ar );
		
		DebugLog.WriteLine( string.Format("RESULT: {0}" , lm.w) );
		DebugLog.WriteLine( string.Format("INFO: {0}" , info ) );
		DebugLog.WriteLine( string.Format("AR: {0}" , ar ) );
		
		double[] testInputs = new double[this.nInputs];
		for(int i=0;i<this.nInputs;i++)
		{
			testInputs[i] = nData[0,i];
		}
		double actualOutput = nData[0, this.nInputs];
		DebugLog.WriteLine( string.Format( "TEST: {0} = {1} (should be {2})" , testInputs , alglib.linreg.lrprocess(lm , testInputs) , actualOutput) );
	}
	
	public double Process(params double[] inputs)
	{
		if(inputs.Length != nInputs)
		{
			throw new System.Exception( "INCORRECT NUMBER OF INPUTS INTO SOLVER: " + inputs.Length.ToString() + " VS " + nInputs.ToString() );
		}
		return alglib.linreg.lrprocess(lm , inputs);
	}
}