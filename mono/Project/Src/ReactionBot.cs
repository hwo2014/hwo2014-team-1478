using System;
using System.Collections;
using System.Collections.Generic;

public class ReactionBot : Bot
{
	public override void OnCrash(CrashResponse cr, int tick) { }
	
	public override void OnSpawn(SpawnResponse sp, int tick) { }
	
	public override BotCommand Update (List<CarPosition> poses, int tick)
	{
		CarPosition currPos = yourCar.Stats.currPosition;
		
		if(currPos == null)
		{
			DebugLog.WriteLine( "Skipped car position tick!" );
			return BotCommand.Throttle(0.5);
		}
		else
		{
			double nextThrottle = 0f;

			//Piece nextPiece = currentRace.GetNextPiece(currPos);
			if( yourCar.Stats.currDriftVelocity > yourCar.Stats.currVelocity  )
			{
				nextThrottle = 0;
			}
			else
			{
				nextThrottle = 1f;
			}
			
			return BotCommand.Throttle(nextThrottle);
		}
	}
}


