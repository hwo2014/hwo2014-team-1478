using System;
using System.IO;
using System.Net.Sockets;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;

public class DebugLog
{
	public static System.Action<string> LoggingFunc = null;
	
	public static void WriteLine(string str)
	{
		if(LoggingFunc == null)
		{
			Console.WriteLine(str);
		}
		else
		{
			LoggingFunc(str);
		}
	}
}

public class BotController
{

	public static void Main(string[] args) 
	{
		string host = args[0];
		int port = int.Parse(args[1]);
		string botName = args[2];
		string botKey = args[3];

		DebugLog.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
		
		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;
			
			BotController bc = new BotController(reader, writer,new LearningBot2());
			bc.Start(new Join(botName, botKey));
		}
	}
	
	private StreamWriter writer;
	private StreamReader reader;
	private Bot bot;

	private Race raceFound;
	private CarId youFound;

	public BotController(StreamReader reader, StreamWriter writer, Bot bot)
	{
		this.writer = writer;
		this.reader = reader;
		this.bot = bot;
	}

	protected void Start(Join join) 
	{
		string line;

		send(join, 0);

		while((line = reader.ReadLine()) != null) {

			var startT = System.DateTime.Now;
			DebugLog.WriteLine( "--- Start ---" );
			DebugLog.WriteLine( string.Format("Recv: {0}", line ) );
			ResponseWrapper msg = JsonConvert.DeserializeObject<ResponseWrapper>(line);
			DebugLog.WriteLine( string.Format("Message Data Type: {0}" , msg.data == null ? "null" : msg.data.GetType().ToString() ) );
			int tick  = msg.gameTick;
			switch(msg.msgType) {
			case "yourCar":
				CarId yc = (msg.data as JObject).ToObject<CarId>();
				DebugLog.WriteLine("Car: " + yc.ToString() );
				youFound = yc;
				if(youFound != null && raceFound != null)
				{
					bot.Init(raceFound, youFound);
				}
				send(new Ping(),tick);
				break;
			case "carPositions":
				List<CarPosition> lst = (msg.data as JArray).ToObject<List<CarPosition>>();
				foreach(CarPosition cp in lst)
				{
					DebugLog.WriteLine("Car Position: " + cp.ToString());
				}
				bot.BeforeUpdate(lst, tick);
				BotCommand cmd = bot.Update(lst, tick);
				bot.AfterUpdate(lst, tick);
				if(cmd.type == BotCommand.Type.Throttle)
				{
					DebugLog.WriteLine( string.Format("Throttle: {0}" , cmd.throttle ) );
					send(new Throttle(cmd.throttle),tick);
				}
				else
				{
					DebugLog.WriteLine( string.Format("Switch: {0}" , cmd.direction ) );
					send( new Switch(cmd.direction == BotCommand.Direction.Left ? "Left" : "Right"), tick );
				}
				break;
			case "join":
				JoinResponse j = (msg.data as JObject).ToObject<JoinResponse>();
				DebugLog.WriteLine(string.Format("Joined: {0}" , j.ToString() ) );
				send(new Ping(),tick);
				break;
			case "gameInit":
				GameInitResponse gi= (msg.data as JObject).ToObject<GameInitResponse>();
				raceFound = gi.race;
				raceFound.Link();
				if(youFound != null && raceFound != null)
				{
					bot.Init(raceFound, youFound);
				}
				DebugLog.WriteLine("Race init: " + gi.ToString() );
				send(new Ping(),tick);
				break;
			case "gameEnd":
				DebugLog.WriteLine("Race ended");
				send(new Ping(),tick);
				break;
			case "gameStart":
				DebugLog.WriteLine("Race starts");
				send(new Ping(),tick);
				break;
			case "spawn":
				SpawnResponse sr = (msg.data as JObject).ToObject<SpawnResponse>();
				bot.OnSpawn(sr, msg.gameTick);
				send(new Ping(),tick);
				break;
			case "crash":
				CrashResponse cr = (msg.data as JObject).ToObject<CrashResponse>();
				bot.OnCrash(cr, msg.gameTick);
				send(new Ping(),tick);
				break;
			default:
				send(new Ping(),tick);
				break;
			}
			DebugLog.WriteLine( string.Format("--- Fin {0} ms ---" , (long)(System.DateTime.Now - startT).TotalMilliseconds ) );
		}
	}
	
	private void send(SendMsg msg, int tick) {
		msg.gameTick = tick;
		string s = msg.ToJson();
		DebugLog.WriteLine("SEND: " + s);
		writer.WriteLine(s);
	}
}


//[JsonConverter(typeof(RecvMsgWrapperConverter))]
class ResponseWrapper 
{
	public string msgType;
	public Object data;
	public string gameId = null;
	public int gameTick = 0;

	public ResponseWrapper(string msgType, Object data) 
	{
		this.msgType = msgType;
		this.data = data;
	}
	

}

public class CarId
{
	public string name;
	public string color;
}

public class JoinResponse
{
	public string name;
	public string key;

	public override string ToString ()
	{
		return string.Format ("[JoinResponse {0} {1}]" , this.name,this.key);
	}
}

public class GameInitResponse
{
	public Race race;

	public override string ToString ()
	{
		return string.Format ("[GameInitResponse {0}]" , race.ToString() );
	}
}

public class CrashResponse
{
	public string name;
	public string color;
}

public class SpawnResponse
{
	public string name;
	public string color;
}

public class GameEndResponse
{
	// TODO
}

public class GameStartResponse
{
	// nothing here
}

/*
public class RecvMsgWrapperConverter : JsonConverter
{
	public override void WriteJson (JsonWriter writer, object value, JsonSerializer serializer)
	{
		throw new NotImplementedException ();
	}

	public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
	{
		if (reader.TokenType == JsonToken.Null)
			return null;

		reader.Read(); // msgType propertyName

		if ((string)reader.Value != "msgType") throw new InvalidOperationException();
		
		reader.Read(); // msgType value
		
		string msgType = (string)reader.Value;
		
		object value;
		
		switch(msgType)
		{
		case "carPositions":
			value = null;
			break;
		case "join":
			value = new JoinMsg();
			break;
		case "gameInit":
			value = new GameInitMsg();
			break;
		case "gameEnd":
			value = new GameEndMsg();
			break;
		case "gameStart":
			value = new GameStartMsg();
			break;
		case "yourCar":
			value = new YourCarMsg();
			break;
		default:
			DebugLog.WriteLine("Message Not Supported: " + msgType);
			value = new object();
			break;
		}
		
		reader.Read(); // data propertyName

		if( (reader.Value as string) != "data" )
		{
			DebugLog.WriteLine( "Weird result: " + reader.Value );
		}

		if( msgType == "carPositions" )
		{
			List<CarPosition> poses = new List<CarPosition>();
			reader.Read();
			if(reader.TokenType != JsonToken.StartArray)
			{
				DebugLog.WriteLine( msgType + "Bad token type: 1" + reader.TokenType.ToString() );
			}

			reader.Read();

			while(reader.TokenType != JsonToken.EndArray)
			{
				if(reader.TokenType != JsonToken.StartObject)
				{
					DebugLog.WriteLine( msgType + "Bad token type: 2" + reader.TokenType.ToString() );
				}
				CarPosition p = new CarPosition();
				serializer.Populate(reader, p);
				poses.Add(p);

				reader.Read();
				if(reader.TokenType != JsonToken.EndObject)
				{
					DebugLog.WriteLine( msgType + "Bad token type end: 3" + reader.TokenType.ToString() );
				}

				reader.Read();

			}

			value = poses;
		}
		else
		{
			reader.Read();
			if(reader.TokenType != JsonToken.StartObject)
			{
				DebugLog.WriteLine( msgType + "Bad token type: 4" + reader.TokenType.ToString() );
			}

			serializer.Populate(reader, value);

			reader.Read();

			if(reader.TokenType != JsonToken.EndObject)
			{
				DebugLog.WriteLine( msgType + "Bad token type normal: 5" + reader.TokenType.ToString() );
			}
		}


		RecvMsgWrapper r = new RecvMsgWrapper(msgType, value);

		return r;
	}
	
	public override bool CanConvert(Type objectType)
	{
		return objectType == typeof(RecvMsgWrapper);
	}
}
*/